/*
 * Aprovador: include helper para criacao de metadados.
 *
 * Parametros:
    - pUid*......: ttDocument.uid
    - pItemId....: ttDocumentItem.itemId.
                   - Para passar o pItemId diretamente, utilize aspas simples, ex.:
                     {create_metadata.i ... &pItemId='10'}
    - pSeqParent.: seqParent do metadado.
    - pMetadataId: id do metadado.
    - pType......: tipo do metadado.
    - pLabel.....: label do metadado.
    - pValue.....: value do metadado
    - pNestedId..: metadataId do nested_list a ser criado pela createMetadataBuffer.
    - pTableId...: metadataId de um tabela a ser criada pelo createMetadataBuffer.
    - pTable.....: Tabela ou temp-table para geracao dos metadados.
                   - Se informado, sera utilizado o createMetadataBuffer, senao, createDocumentMetadata_v2
                     ou createItemMetadata_v2
    - pTableHeaders: Lista de cabecalhos da tabela separados por virgula. Caso nao seja preenchido, sera verificado nas se ha um cabecalho padrao
    - pFields....: Lista de campos separado por virgula, se nao for preenchido, todos os campos serao gerados.
                   - Para passar uma lista de campos diretamente, utilize aspas simples, ex.:
                     {create_metadata.i ... &pFields='campo-a,campo-b'}
    - pTableName.: Nome da tabela para serializar o metadatado, se nao informado serialize-name sera utilizado.
    - pLang......: Idioma (i18n.i LANG_<lang>). Se nao informado, sera LANG_PTBR
    - pOutSeq....: Variavel para receber o output pSeq na criacao do metadado.
                   - Se nao informado, sera iSeqM__

   Obsservacoes:
    - * obrigatorios
 */

&IF defined(CREATE-METADATA) = 0 &THEN
    &global-define CREATE-METADATA 
   
    def var iSeqM__ as int no-undo.
&ENDIF

&IF defined(DOCUMENTS) = 0 &THEN
    {include/documents.i}
&ENDIF

&IF defined(pUid) = 2 AND defined(pTable) = 2 &THEN
    run createMetadataBuffer(input {&pUid},
                             input &IF defined(pItemId)       = 2 &THEN {&pItemId}       &ELSE ""             &ENDIF,
                             input &IF defined(pSeqParent)    = 2 &THEN {&pSeqParent}    &ELSE 0              &ENDIF,
                             input &IF defined(pNestedId)     = 2 &THEN {&pNestedId}     &ELSE ""             &ENDIF,
                             input &IF defined(pTableId)      = 2 &THEN {&pTableId}      &ELSE ""             &ENDIF,
                             input &IF defined(pTableHeaders) = 2 &THEN {&pTableHeaders} &ELSE ""             &ENDIF,
                             input buffer {&pTable}:handle,
                             input &IF defined(pFields)       = 2 &THEN {&pFields}       &ELSE ""             &ENDIF,
                             input &IF defined(pRawFields)    = 2 &THEN {&pRawFields}    &ELSE ""             &ENDIF,
                             input &IF defined(pTableName)    = 2 &THEN {&pTableName}    &ELSE ""             &ENDIF,
                             input &IF defined(pLang)         = 2 &THEN {&pLang}         &ELSE "{&LANG_PTBR}" &ENDIF).

&ELSEIF defined(pUid) = 2 AND defined(pItemId) = 2 &THEN
    if {&pItemId} = "" then do:
        run createDocumentMetadata_v2(input  {&pUid},
                                  input  &IF defined(pSeqParent)  = 2 &THEN {&pSeqParent}   &ELSE 0       &ENDIF,
                                  input  &IF defined(pMetadataId) = 2 &THEN {&pMetadataId}  &ELSE ""      &ENDIF,
                                  input  &IF defined(pType)       = 2 &THEN {&pType}        &ELSE ""      &ENDIF,
                                  input  &IF defined(pLabel)      = 2 &THEN {&pLabel}       &ELSE ""      &ENDIF,
                                  input  &IF defined(pValue)      = 2 &THEN {&pValue}       &ELSE ""      &ENDIF,
                                  input  "",
                                  output &IF defined(pOutSeq)     = 2 &THEN {&pOutSeq}      &ELSE iSeqM__ &ENDIF).
    end.
    else do:
        run createItemMetadata_v2(input  {&pUid},
                                  input  &IF defined(pItemId)     = 2 &THEN {&pItemId}      &ELSE ""      &ENDIF,
                                  input  &IF defined(pSeqParent)  = 2 &THEN {&pSeqParent}   &ELSE 0       &ENDIF,
                                  input  &IF defined(pMetadataId) = 2 &THEN {&pMetadataId}  &ELSE ""      &ENDIF,
                                  input  &IF defined(pType)       = 2 &THEN {&pType}        &ELSE ""      &ENDIF,
                                  input  &IF defined(pLabel)      = 2 &THEN {&pLabel}       &ELSE ""      &ENDIF,
                                  input  &IF defined(pValue)      = 2 &THEN {&pValue}       &ELSE ""      &ENDIF,
                                  input  "",
                                  output &IF defined(pOutSeq)     = 2 &THEN {&pOutSeq}      &ELSE iSeqM__ &ENDIF).
    end.
&ELSEIF defined(pUid) = 2 &THEN
    run createDocumentMetadata_v2(input  {&pUid},
                                  input  &IF defined(pSeqParent)  = 2 &THEN {&pSeqParent}   &ELSE 0       &ENDIF,
                                  input  &IF defined(pMetadataId) = 2 &THEN {&pMetadataId}  &ELSE ""      &ENDIF,
                                  input  &IF defined(pType)       = 2 &THEN {&pType}        &ELSE ""      &ENDIF,
                                  input  &IF defined(pLabel)      = 2 &THEN {&pLabel}       &ELSE ""      &ENDIF,
                                  input  &IF defined(pValue)      = 2 &THEN {&pValue}       &ELSE ""      &ENDIF,
                                  input  "",
                                  output &IF defined(pOutSeq)     = 2 &THEN {&pOutSeq}      &ELSE iSeqM__ &ENDIF).

&ENDIF

