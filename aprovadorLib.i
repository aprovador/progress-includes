/**
 * Aprovador - Helper functions
 */
 
&global-define APROVADOR-LIB

{include/global.i}

function A_dateFormat returns character (pDate as date, pTime as int):
    define variable cRetorno as character.

    if pDate = ? then do:
        return "".
    end.
	
    assign cRetorno = string(year(pDate),"9999") + "-" + 
					  string(month(pDate),"99")  + "-" + 
					  string(day(pDate),"99")    + "T" +
					  string(pTime, "HH:MM:SS")  +
                      string(timezone, "+HH:MM").
	return cRetorno.
end function.

function A_dateFormat2 returns character (pDate as date, pTime as char):
    define variable cRetorno as character.

    if pDate = ? then do:
        return "".
    end.
	
    assign cRetorno = string(year(pDate),"9999") + "-" + 
					  string(month(pDate),"99")  + "-" + 
					  string(day(pDate),"99")    + "T" +
					  trim(pTime) +
                      string(timezone, "+HH:MM").
	return cRetorno.
end function.

function A_strDateEn returns char (pDate as date):
    if pDate = ? then do:
        return "".
    end.
    return string(month(pDate)) + "/" + string(day(pDate)) + "/" + string(year(pDate)).
end.

function A_cgcFormat returns character (pCGC as char):
    def var c-format as char no-undo.

    case length(pCGC):
        when 11 /* CPF */  then assign c-format = string(pCGC, "999.999.999-99") no-error.
        when 14 /* CNPJ */ then assign c-format = string(pCGC, "99.999.999/9999-99") no-error.
        otherwise do:
            assign c-format = pCGC.
        end.
    end case.

    return c-format.
end.

function A_moneyFormat returns character (pValue as dec):
    return trim(string(pValue, ">>>,>>>,>>9.99")).
end.


function A_toUTF8 returns character (pChar as char):
    return codepage-convert(pChar, "utf-8").
end.

function A_replaceN returns character (pChar as char):
    return replace( replace(pChar, chr(10), " "), chr(13), " ").
end.

function A_base64Encode returns char (pChar as char):
    def var rString  as raw  no-undo.
    def var cEncode  as char no-undo.

    put-string(rString, 1, length(pChar)) = pChar.
    assign cEncode = base64-encode(rString).
    
    return cEncode.
end.

/* Se pChar nao for uma string base64 valida, retorna pChar. */
function A_base64Decode returns char (pChar as longchar):
    def var mDecoded as memptr   no-undo.
    def var lcReturn as longchar no-undo.
    def var cReturn  as char     no-undo.

    assign mDecoded = base64-decode(pChar) no-error.
    if mDecoded = ? then do:
        return string(pChar).
    end.

    copy-lob from mDecoded to lcReturn.
    assign cReturn = lcReturn.

    return cReturn.
end.

procedure A_bufferToUTF8:
    def input parameter buf as handle no-undo.

    def var iCont  as int    no-undo.
    def var hField as handle no-undo.

    do iCont = 1 to buf:num-fields:
        hField = buf:buffer-field(iCont).
        if hField:data-type = "character" then do:
            hField:buffer-value = A_toUTF8(hField:buffer-value).
        end.
    end.
end.


procedure A_tableToUTF8:
    def input parameter pBuf as handle no-undo.

    def var hQry as handle no-undo.
    
    create query hQry.
    hQry:set-buffers(pBuf).
    hQry:query-prepare("for each " + pBuf:name).
    
    hQry:query-open().
    do while hQry:get-next():
        run A_bufferToUTF8(input pBuf).
    end.
    hQry:query-close().
end.

procedure A_writeLog:
    def input parameter pMessage as char no-undo.

    def var cLog as char no-undo.

    assign cLog = this-procedure:file-name +
                  ": " +
                  pMessage.

    if session:remote then do:
        /* Execucao via appserver - escreve mensagem no log do agent */
        log-manager:write-message(cLog, "APVDR").
    end.
    else do:
        put unformatted
            string(now)
            " "
            cLog
            skip.
    end.
end.

procedure A_getError:
    def output parameter pError as char no-undo.

    def var iErr as int no-undo.

    if error-status:error then do:
        do iErr = 1 to error-status:num-messages:
            pError = pError + ". " +
                     error-status:get-message(iErr).
        end.
    end.

    pError = trim(pError, ". ").
end.

function A_removeDuplicates returns char (pStrChar as char, pStrSeparator as char):
    def var iCont       as int  no-undo.
    def var cStr        as char no-undo.
    def var cAux        as char no-undo.
    
    do iCont = 1 to num-entries(pStrChar, pStrSeparator):
        assign cAux = entry(iCont, pStrChar, pStrSeparator).
        if lookup(cAux, cStr, pStrSeparator) = 0 then do:
            assign cStr = cStr + "," + cAux.
        end.
    end.
    cStr = trim(cStr, pStrSeparator).  
    return cStr.
end.

function A_decimalFormat returns char (pValue as dec):
    def var cFormat   as char no-undo.
    def var iTam      as int  no-undo.
    def var cDec      as char no-undo.
    def var dDec      as dec  no-undo.
    def var dIntValue as dec  no-undo.

    assign dIntValue = truncate(pValue, 0)
           dDec = pValue - dIntValue.

    if dDec <> 0 then do:
        
        assign cDec = entry(2, string(pValue), session:numeric-decimal-point)
               iTam = length(cDec).

        case iTam:
            when 1 or when 2 then do:
                assign cFormat = "->>,>>>,>>>,>>9.99".
            end.
            when 3 then do:
                assign cFormat = "->>,>>>,>>>,>>9.999".
            end.
            when 4 then do:
                assign cFormat = "->>,>>>,>>>,>>9.9999".
            end.
            otherwise do:
                assign cFormat = "->>,>>>,>>>,>>9.99999".
            end.
        end case.
        return cFormat.
    end.
    else do:
        cFormat = "->>,>>>,>>>,>>9.99".
        return cFormat.
    end.
end.

function isCompanyEnabled returns log (pCompaniesList as char,
                                       pCompany as char):
    if pCompaniesList = "" then do:
        return true.
    end.
    return lookup(pCompany, pCompaniesList) > 0.
end.
