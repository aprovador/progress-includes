/**
 * Aprovador
 *
 */

&GLOBAL-DEFINE DOCUMENTS-LEGACY

&IF defined(DOCUMENTS) = 0 &THEN
    {include/documents.i}
&ENDIF 

/* Procedures */
procedure createMetadata:
    def input  parameter pUid        as char no-undo.
    def input  parameter pItemId     as char no-undo.
    def input  parameter pMetadataId as char no-undo.
    def input  parameter pType       as char no-undo.
    def input  parameter pLabel      as char no-undo.
    def input  parameter pValue      as char no-undo.
    def output parameter pSeq        as int  no-undo init {&METADATA_ADD_SEQ}.

    if trim(pUid) = "" or trim(pValue) = "" then do:
        return.
    end.

    if pItemId = "" then do:
        run createDocumentMetadataId(input  pUid,
                                     input  pMetadataId,
                                     input  pType,
                                     input  pLabel,
                                     input  pValue,
                                     output pSeq).
    end.
    else do:
        run createItemMetadataId(input  pUid,
                                 input  pItemId,
                                 input  pMetadataId,
                                 input  pType,
                                 input  pLabel,
                                 input  pValue,
                                 output pSeq).
    end.
end.

procedure createDocumentMetadata:
    def input  parameter pUid    as char no-undo.
    def input  parameter pType   as char no-undo.
    def input  parameter pLabel  as char no-undo.
    def input  parameter pValue  as char no-undo.
    def output parameter pSeq    as int  no-undo init {&METADATA_ADD_SEQ}.

    def buffer bTtDocumentMetadata for ttDocumentMetadata.

    if trim(pUid) = "" or trim(pValue) = "" then do:
        return.
    end.

    for last bTtDocumentMetadata no-lock
        where bTtDocumentMetadata.docUid = pUid:
        assign pSeq = bTtDocumentMetadata.seq + {&METADATA_ADD_SEQ}.
    end.

    if pType = "" then do:
        assign pType = "string".
    end.

    create ttDocumentMetadata.
    assign ttDocumentMetadata.docUid     = pUid
           ttDocumentMetadata.seq        = pSeq
           ttDocumentMetadata.fieldType  = pType
           ttDocumentMetadata.fieldLabel = pLabel
           ttDocumentMetadata.fieldValue = trim(pValue).
end.

procedure createItemMetadata:
    def input  parameter pUid    as char no-undo.
    def input  parameter pItemId as char no-undo.
    def input  parameter pType   as char no-undo.
    def input  parameter pLabel  as char no-undo.
    def input  parameter pValue  as char no-undo.
    def output parameter pSeq    as int  no-undo init {&METADATA_ADD_SEQ}.

    def buffer bTtDocumentItemMetadata for ttDocumentItemMetadata.

    if pUid = "" or pItemId = "" then do:
        return.
    end.

    for last bTtDocumentItemMetadata no-lock
        where bTtDocumentItemMetadata.docUid = pUid
          and bTtDocumentItemMetadata.itemId = pItemId:
        assign pSeq = bTtDocumentItemMetadata.seq + {&METADATA_ADD_SEQ}.
    end.

    if pType = "" then do:
        assign pType = "string".
    end.

    create ttDocumentItemMetadata.
    assign ttDocumentItemMetadata.docUid     = pUid
           ttDocumentItemMetadata.itemId     = pItemId
           ttDocumentItemMetadata.seq        = pSeq
           ttDocumentItemMetadata.fieldType  = pType
           ttDocumentItemMetadata.fieldLabel = pLabel
           ttDocumentItemMetadata.fieldValue = trim(pValue).
end.

procedure createDocumentMetadataId:
    def input  parameter pUid        as char no-undo.
    def input  parameter pMetadataId as char no-undo.
    def input  parameter pType       as char no-undo.
    def input  parameter pLabel      as char no-undo.
    def input  parameter pValue      as char no-undo.
    def output parameter pSeq        as int  no-undo.

    run createDocumentMetadata(input  pUid,
                               input  pType,
                               input  pLabel,
                               input  pValue,
                               output pSeq).

    if pMetadataId <> "" then do:
        for first ttDocumentMetadata
            where ttDocumentMetadata.docUid = pUid
              and ttDocumentMetadata.seq    = pSeq:
            assign ttDocumentMetadata.metadataId = pMetadataId.
        end.
    end.
end.

procedure createItemMetadataId:
    def input  parameter pUid        as char no-undo.
    def input  parameter pItemId     as char no-undo.
    def input  parameter pMetadataId as char no-undo.
    def input  parameter pType       as char no-undo.
    def input  parameter pLabel      as char no-undo.
    def input  parameter pValue      as char no-undo.
    def output parameter pSeq        as int  no-undo.

    run createItemMetadata(input  pUid,
                           input  pItemId,
                           input  pType,
                           input  pLabel,
                           input  pValue,
                           output pSeq).

    if pMetadataId <> "" then do:
        for first ttDocumentItemMetadata
            where ttDocumentItemMetadata.docUid = pUid
              and ttDocumentItemMetadata.itemId = pItemId
              and ttDocumentItemMetadata.seq    = pSeq:
            assign ttDocumentItemMetadata.metadataId = pMetadataId.
        end.
    end.
end.

procedure createDocumentItem:
    def input parameter pDocUid   as char no-undo.
    def input parameter pId       as char no-undo.
    def input parameter pTextId   as char no-undo.
    def input parameter pTextName as char no-undo.

    if can-find(first ttDocumentItem
                    where ttDocumentItem.docUid = pDocUid
                      and ttDocumentItem.id     = pId) then do:
        return.
    end.

    create ttDocumentItem.
    assign ttDocumentItem.docUid   = pDocUid
           ttDocumentItem.id       = pId
           ttDocumentItem.textId   = pTextId
           ttDocumentItem.textName = pTextName.
end.

procedure createDocumentTag:
    def input parameter pUid   as char no-undo.
    def input parameter pLabel as char no-undo.

    def buffer bTtDocumentTag for ttDocumentTag.
    def var iSeq as int no-undo init 1.

    if pUid = "" or pLabel = "" then do:
        return.
    end.

    pLabel = trim(pLabel).

    if can-find(first ttDocumentTag no-lock
                    where ttDocumentTag.docUid   = pUid
                      and ttDocumentTag.tagLabel = pLabel) then do:
        return.
    end.

    for last bTtDocumentTag no-lock
        where bTtDocumentTag.docUid = pUid:
        iSeq = bTtDocumentTag.seq + 1.
    end.

    create ttDocumentTag.
    assign ttDocumentTag.docUid   = pUid
           ttDocumentTag.seq      = iSeq
           ttDocumentTag.tagLabel = pLabel.
end.
