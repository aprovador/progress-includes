/**
 * Aprovador Sistemas LTDA - 2015
 * All rights reserved.
 *
 * datasulRest.i
 */
 
&global-define DATASUL-REST

def temp-table ttRestParams no-undo 
    field webServiceURL	   as char serialize-hidden
    field auth             as char serialize-hidden
    field downloadFromRest as log  serialize-hidden
    field downloadPageSize as int  serialize-hidden.
