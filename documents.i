/**
 * Aprovador - Documents definitions
 *
 * Observacoes:
 * - Definicoes que utilizam o sufixo v_<versao> existem para preservar a 
 *   compatiblidade com customizacoes nao atualizadas, e devem ser removidas
 *   a medida que os clientes atualizarem suas customizacoes.
 */

&IF defined(I18N) = 0 &THEN
    {include/i18n.i}
&ENDIF

&IF defined(APROVADOR-LIB) = 0 &THEN
    {include/aprovadorlib.i}
&ENDIF

&global-define DOCUMENTS
&global-define MAX_ITEMS_COUNT 250
&global-define METADATA_ADD_SEQ 10

def var STATUS_PENDING  as char no-undo init "pending".
def var STATUS_APPROVED as char no-undo init "approved".
def var STATUS_REJECTED as char no-undo init "rejected".

define temp-table ttAprovadorUser no-undo serialize-name "users"
    field aprovadorUser      as character
    field language           as character
    index pk as primary unique
          aprovadorUser.

define temp-table ttPackage no-undo serialize-name "packages"
    field paramId               as int  serialize-hidden
    field id                    as char
    field description           as char
    field sinceDateStr          as char serialize-name "sinceDate"
    field sinceDate             as date serialize-hidden
    field purchaseHistory       as log  init no
    field purchaseHistoryCotac  as log  init no
    field purchaseHistoryDate   as date
    field mpdSitAvaliado        as log  init no
    field sendRejectedAsPending as log  init no
    field approvalHistory       as log  init no
    field profitability         as log  init no
    field MLACheckPermission    as log  init no
    field openInvoices          as log  init no
    field consumptionData       as log  init no
    field confInstallments      as log  init no
    field installmentsDate      as date
    field MLAOnlyIds            as char
    field MLAExceptIds          as char
    field useCache              as log  init no
    field cacheMinutes          as int  
    field pageLimit             as int          serialize-hidden
    field pageLastId            as char         serialize-hidden
    field isLastpage            as log  init no serialize-hidden
    index pk as primary unique
          id.

define temp-table ttDocument no-undo serialize-name "documents"
    field uid                as character format "x(1000)" serialize-hidden 
    field companyId          as character format "x(10)"                
    field documentId         as character format "x(30)"   serialize-name "documentId"
    field type               as character format "x(30)"   serialize-name "type" /* compras, mla, pedidos, etc */	
    field aprovadorRequester as character format "x(2000)" 
    field aprovadorUser      as character format "x(200)" 
    field layoutId           as character format "x(30)" 
    field creationDate       as character format "x(25)"  
    field dueDate            as character format "x(30)"  
    field textFrom           as character format "x(200)" 
    field textSubject        as character format "x(200)" 
    field textMessage        as character format "x(4000)" 
    field textValue          as character format "x(100)"  
    field textParty          as character format "x(100)" 
    field textCostCenter     as character format "x(100)" 
    field textPaymentTerms   as character format "x(100)" 
    field labelValue         as character format "x(100)"  init "Valor Total"
    field textId             as character format "x(100)" 
    field documentValue      as character format "x(100)"  serialize-name "value" 
    field companyName        as character format "x(100)" 
    field statusCode		 as character                  /* pending, approved, rejected */
    field isAlternative      as logical                    init no
    field delaySync          as logical                    init no
    field externalId         as character format "x(100)"  serialize-name "externalId"
    index pk as primary unique
          uid
    index pu as unique
          companyId
          documentId
          type
          aprovadorUser
    index username
          aprovadorUser
    index company
          companyId.

/**
 * Estrutura base de um metadado
 *
 * Obs.: fieldLabel nao pode estar no indice, senao o write json
 *       serializa a metadata ordenado por nome, e nao na ordem
 *       em que os registros foram criados.
 *       Campo "seq" utilizado para garantir que o write-json sempre ira gerar
 *       os metadados na mesma ordem.
 */
def temp-table ttMetadata no-undo 
    field docUid	    	as character                  serialize-hidden
    field seq               as int                        serialize-hidden
    field fieldType         as character format "x(20)"   serialize-name "type"
	field fieldLabel		as character format "x(100)"  serialize-name "label"
    field fieldValue		as character format "x(5000)" serialize-name "value"
    field fieldRawValue     as character format "x(50)"   serialize-name "rawValue"
    field metadataId        as character format "x(100)"  serialize-name "id"
    field seqParent         as int                        serialize-hidden.

define temp-table ttDocumentMetadata no-undo serialize-name "metadata" like ttMetadata
    index pk as primary unique
          docUid
          seq
    index metadata
          docUid
          metadataId
    index chParent
          seqParent.

define temp-table ttDocumentItem no-undo serialize-name "items"
    field docUid            as character format "x(100)"  serialize-hidden
    field id                as character format "x(30)"                
	field textId            as character format "x(30)"                
    field textName          as character format "x(2000)"                
    field collectionId      as character format "x(30)"   init "item"
    field seq               as int
    index pk as primary unique
          docUid
          id
    index seqOrder
          seq.

def temp-table ttDocumentItemMetadata no-undo serialize-name "metadata" like ttMetadata
    field itemId			as character format "x(100)" serialize-hidden
    index pk as primary unique
          docUid
          itemId
          seq
    index metadata
          docUid
          itemId
          metadataId
    index chParent
          seqParent.

def temp-table ttTableMetadata no-undo serialize-name "table" 
    field docUid	    	as character                  serialize-hidden
    field itemId			as character format "x(100)"  serialize-hidden
    field tableRow          as int                        serialize-hidden
    field tableValue		as character format "x(2000)" serialize-name "value"
    field seq               as int                        serialize-hidden
    field seqParent         as int                        serialize-hidden
    index pk as primary unique
          docUid
          itemId
          seqParent
          tableRow
          seq.

/**
 * Obs.: - assim como nos metadados, seq garante a sequencia dos registros no write-json.
 *       - o sufixo _obj indica para o Conector que o parse de tags eh por array de objetos.
 */
def temp-table ttDocumentTag no-undo serialize-name "tags_obj"
    field docUid   as char                 serialize-hidden
    field tagLabel as char format "x(100)" serialize-name "label" 
    field seq      as int                  serialize-hidden
    index pu as primary unique
          docUid
          tagLabel
          seq.

def temp-table ttDocumentAttachment no-undo serialize-name "attachments"
    field docUid           as char serialize-hidden
    field seq              as int  serialize-hidden
    field description      as char format "x(1000)"
    field path             as char format "x(500)"
    field url              as char format "x(300)"
    field rawHeaders       as char format "x(300)"
    field lastModified     as char format "x(30)"
    field downloadPageSize as int
    index pu as primary unique
          docUid
          seq
          path.

define temp-table ttPendingDocument no-undo serialize-name "documents"
    field aprovadorId       as character
    field companyId			as character
    field documentId   		as character
    field type	        	as character
    field aprovadorUser     as character
    field statusCode 		as character
    field statusDescription as character
    index p as primary unique
          companyId
          documentId
          type
          aprovadorUser
    index aprovador as unique
          aprovadorId
    index company
          companyId.

define temp-table ttProcessedDocument no-undo serialize-name "documents"
    field aprovadorId     as character
    field companyId		  as character
    field documentId   	  as character
    field type	          as character
    field aprovadorUser   as character
    field statusCode	  as character
    field approvalError	  as character
    field erpApprovalDate as character
    field approvalNotice  as character
    field isLastStep      as log       init yes
    index p as primary unique
          companyId
          documentId
          type
          aprovadorUser
    index aprovador as unique
          aprovadorId.

/* Procedures */
procedure createDocumentMetadata_v2:
    def input  parameter pUid        as char no-undo.
    def input  parameter pSeqParent  as int  no-undo.
    def input  parameter pMetadataId as char no-undo.
    def input  parameter pType       as char no-undo.
    def input  parameter pLabel      as char no-undo.
    def input  parameter pValue      as char no-undo.
    def input  parameter pRawValue   as char no-undo.
    def output parameter pSeq        as int  no-undo init {&METADATA_ADD_SEQ}.
 
    def buffer bTtDocumentMetadata for ttDocumentMetadata.

    if trim(pUid) = "" then do:
        return.
    end.

    if pType = "" then do:
        assign pType = "string".
    end.

    for last bTtDocumentMetadata no-lock
        where bTtDocumentMetadata.docUid = pUid:

        assign pSeq = bTtDocumentMetadata.seq + {&METADATA_ADD_SEQ}.
    end.

    create ttDocumentMetadata.
    assign ttDocumentMetadata.docUid        = pUid
           ttDocumentMetadata.seqParent     = pSeqParent
           ttDocumentMetadata.seq           = pSeq
           ttDocumentMetadata.metadataId    = pMetadataId
           ttDocumentMetadata.fieldType     = pType
           ttDocumentMetadata.fieldLabel    = pLabel
           ttDocumentMetadata.fieldValue    = trim(pValue).

    if pRawValue <> "" then do:

        assign ttDocumentMetadata.fieldRawValue = pRawValue. 
    end.
end.

procedure createItemMetadata_v2:
    def input  parameter pUid        as char no-undo.
    def input  parameter pItemId     as char no-undo.
    def input  parameter pSeqParent  as int  no-undo.
    def input  parameter pMetadataId as char no-undo.
    def input  parameter pType       as char no-undo.
    def input  parameter pLabel      as char no-undo.
    def input  parameter pValue      as char no-undo.
    def input  parameter pRawValue   as char no-undo.
    def output parameter pSeq        as int  no-undo init {&METADATA_ADD_SEQ}.
    
    def buffer bTtDocumentItemMetadata       for ttDocumentItemMetadata.

    if pUid = "" or pItemId = "" then do:
        return.
    end.

    if pType = "" then do:
        assign pType = "string".
    end.
    
    for last bTtDocumentItemMetadata no-lock
        where bTtDocumentItemMetadata.docUid = pUid
          and bTtDocumentItemMetadata.itemId = pItemId:

        assign pSeq = bTtDocumentItemMetadata.seq + {&METADATA_ADD_SEQ}.
    end.

    create ttDocumentItemMetadata.
    assign ttDocumentItemMetadata.docUid        = pUid
           ttDocumentItemMetadata.itemId        = pItemId
           ttDocumentItemMetadata.seqParent     = pSeqParent
           ttDocumentItemMetadata.seq           = pSeq
           ttDocumentItemMetadata.metadataId    = pMetadataId
           ttDocumentItemMetadata.fieldType     = pType
           ttDocumentItemMetadata.fieldLabel    = pLabel
           ttDocumentItemMetadata.fieldValue    = trim(pValue).

    if pRawValue <> "" then do:

        assign ttDocumentItemMetadata.fieldRawValue = pRawValue.
    end.
end.

procedure createTableMetadata:
    def input  parameter pUid        as char no-undo.
    def input  parameter pItemId     as char no-undo.
    def input  parameter pSeqParent  as int  no-undo.
    def input  parameter pTableRow   as int  no-undo.
    def input  parameter pValue      as char no-undo.

    def var iSeq as int no-undo init 1.
        
    if trim(pUid) = "" then do:
        return.
    end.

    for last ttTableMetadata no-lock
        where ttTableMetadata.docUid   = pUid
          and ttTableMetadata.itemId   = pItemId
          and ttTableMetadata.tableRow = pTableRow:

        assign iSeq = ttTableMetadata.seq + iSeq.
    end.

    create ttTableMetadata.
    assign ttTableMetadata.docUid     = pUid
           ttTableMetadata.itemId     = pItemId
           ttTableMetadata.seq        = iSeq
           ttTableMetadata.seqParent  = pSeqParent
           ttTableMetadata.tableRow   = pTableRow
           ttTableMetadata.tableValue = trim(pValue).
end.

/* Helper para criacao de metadados a partir de um buffer (temp-table ou tabela) */
procedure createMetadataBuffer:
    def input  parameter pUid          as char   no-undo.
    def input  parameter pItemId       as char   no-undo.
    def input  parameter pSeqParent    as int    no-undo.
    def input  parameter pNestedId     as char   no-undo. /* se informado, pSeqParent eh ignorado */
    def input  parameter pTableId      as char   no-undo. /* Id do metadado de tabela */
    def input  parameter pTableHeaders as char   no-undo. /* cabecalho dos metadados de tabelas de customizados */
    def input  parameter pBufHandle    as handle no-undo.
    def input  parameter pFields       as char   no-undo. /* para metadados de tabela, define as colunas que serao geradas */
    def input  parameter pRawFields    as char   no-undo.
    def input  parameter pTableName    as char   no-undo.
    def input  parameter pLanguage     as char   no-undo.

    def var hField      as handle no-undo.
    def var iField      as int    no-undo.
    def var iExt        as int    no-undo.
    def var cMetadataId as char   no-undo.
    def var cValue      as char   no-undo.
    def var iSeq        as int    no-undo.
    def var cTableName  as char   no-undo.
    def var cRawValue   as char   no-undo.
    def var cColumnName as char   no-undo.
    def var iTableRow   as int    no-undo init 1.
    def var iLastRow    as int    no-undo init 0.
    def var iLastColumn as int    no-undo init 1.

    if not pBufHandle:available then do:
        return.
    end.

    /* Cria nested_list se informado, substituindo o pSeqParent */
    if pNestedId <> "" then do:
        if pItemId = "" then do:
            run createDocumentMetadata_v2(pUid, 0, pNestedId, "nested_list", "", "", "", output pSeqParent).
        end.
        else do:
            run createItemMetadata_v2(pUid, pItemId, 0, pNestedId, "nested_list", "", "", "", output pSeqParent).
        end.
    end.

    if pTableId <> "" then do:
        if pItemId = "" then do:
            
            for first ttDocumentMetadata no-lock
                where ttDocumentMetadata.docUid     = pUid
                  and ttDocumentMetadata.metadataId = pTableId:

                assign pSeqParent = ttDocumentMetadata.seq.
            end.

            if not avail ttDocumentMetadata then do:

                run createDocumentMetadata_v2(pUid, 0, pTableId, "table", "", "", "", output pSeqParent).
            end.
            
        end.
        else do:

            for first ttDocumentItemMetadata no-lock
                where ttDocumentItemMetadata.docUid     = pUid
                  and ttDocumentItemMetadata.itemId     = pItemId
                  and ttDocumentItemMetadata.metadataId = pTableId:

                assign pSeqParent = ttDocumentItemMetadata.seq.
            end.

            if not avail ttDocumentItemMetadata then do:

                run createItemMetadata_v2(pUid, pItemId, 0, pTableId, "table", "", "", "", output pSeqParent).
            end.
        end.

        for last ttTableMetadata no-lock
            where ttTableMetadata.docUid    = pUid
              and ttTableMetadata.itemId    = pItemId
              and ttTableMetadata.seqParent = pSeqParent:
                              
            assign iLastRow = ttTableMetadata.tableRow.
        end.

        if iLastRow = 0 then do:

            assign iLastRow = 1.
        end.
    end.

    if pTableName <> "-" then do:

        if pTableName <> "" then do:

            assign cTableName = pTableName + ".".
        end.
        else do:

            assign cTableName = pBufHandle:serialize-name + ".".
        end. 
    end.

    if pLanguage = "" then do:
        assign pLanguage = "{&LANG_PTBR}".
    end.

    field-block:
    do iField = 1 to pBufHandle:num-fields:
        assign hField = pBufHandle:buffer-field(iField).

        if not hField:available then do:
            next.
        end.

        if pFields <> "" and lookup(hField:name, pFields) = 0 then do:
            next.
        end.

        do iExt = (if hField:extent = 0 then 0 else 1) to hField:extent:

            if hField:buffer-value(iExt) = ? 
                and pTableId = "" then do:
                next.
            end.

            case hField:data-type:
                when "character" then do:
                    assign cValue = trim(hField:string-value(iExt)).
                end.
                when "integer" or when "int64" or when "logical" or when "decimal" then do:
                    assign cValue    = string(hField:buffer-value(iExt), hField:format).
                end.
                when "date" then do:
                    assign cValue    = i18n_dateFormat(pLanguage, date(hField:buffer-value(iExt))).
                end.
                when "datetime" or when "datetime-tz" then do:
                    assign cValue = i18n_datetimeFormat(pLanguage, datetime(hField:buffer-value(iExt))).
                end.
                otherwise do:
                    next field-block.
                end.
            end.
            
            assign cRawValue = "".

            if pRawFields <> "" and lookup(hField:name, pRawFields) > 0  then do:

                if hField:data-type = "date" then do:

                    assign cRawValue = A_dateFormat(date(hField:buffer-value(iExt)), 0).
                end.
            end.
            
            if pTableId <> "" then do:
               
                if iLastRow = 1 then do:
                    
                    if pTableHeaders = "" then do:
                    
                        assign cColumnName = getI18nString(input pLanguage,
                                                           input "tables." + pBufHandle:serialize-name + "." + hField:serialize-name).
                    end.
                    else do:
                        
                        assign cColumnName = entry(iLastColumn, pTableHeaders)
                               iLastColumn = iLastColumn + 1.
                    end.

                    run createTableMetadata(input pUid,
                                            input pItemId,
                                            input pSeqParent,
                                            input iLastRow,
                                            input cColumnName).

                    assign iTableRow = 2.
                end.
                else do:
                    
                    assign iTableRow = iLastRow + 1.
                end.
                                          
                run createTableMetadata(input pUid,
                                        input pItemId,
                                        input pSeqParent,
                                        input iTableRow,
                                        input cValue).
            end.
            else do:

                if trim(cValue) = "" then do:
                    next.
                end.
                
                assign cMetadataId = cTableName + hField:serialize-name + (if iExt = 0 then "" else "_" + string(iExt)).
                
                if pItemId = "" then do:
                    run createDocumentMetadata_v2(pUid, pSeqParent, cMetadataId, "", "", trim(cValue), cRawValue, output iSeq).
                end.
                else do:
                    run createItemMetadata_v2(pUid, pItemId, pSeqParent, cMetadataId, "", "", trim(cValue), cRawValue, output iSeq).
                end.
            end.
        end.
    end.

end.

procedure createDocumentItem_v2:
    def input parameter pDocUid       as char no-undo.
    def input parameter pId           as char no-undo.
    def input parameter pTextId       as char no-undo.
    def input parameter pTextName     as char no-undo.
    def input parameter pCollectionId as char no-undo.
    
    def buffer bTtDocumentItem for ttDocumentItem.

    def var iSeq as int no-undo init 1.

    if can-find(first ttDocumentItem
                    where ttDocumentItem.docUid = pDocUid
                      and ttDocumentItem.id     = pId) then do:
        return.
    end.
    
    for last bTtDocumentItem use-index seqOrder
        where bTtDocumentItem.docUid = pDocUid:

        assign iSeq = bTtDocumentItem.seq + 1.
    end.

    create ttDocumentItem.
    assign ttDocumentItem.docUid       = pDocUid
           ttDocumentItem.id           = pId
           ttDocumentItem.textId       = pTextId
           ttDocumentItem.textName     = pTextName
           ttDocumentItem.seq          = iSeq
           ttDocumentItem.collectionId = if pCollectionId = "" then "Item" else pCollectionId.
end.

procedure createDocumentAttachment:
    def input parameter pUid         as char no-undo.
    def input parameter pDescription as char no-undo.
    def input parameter pPath        as char no-undo.

    def buffer bTtDocumentAttachment for ttDocumentAttachment.
    def var iSeq as int no-undo init 1.

    if pUid = "" or pDescription = "" or pPath = "" then do:
        return.
    end.

    if can-find(first ttDocumentAttachment no-lock
                    where ttDocumentAttachment.docUid = pUid
                      and ttDocumentAttachment.path   = pPath) then do:
        return.
    end.

    for last bTtDocumentAttachment
        where bTtDocumentAttachment.docUid = pUid:
        iSeq = bTtDocumentAttachment.seq + 1.
    end.

    create ttDocumentAttachment.
    assign ttDocumentAttachment.docUid      = pUid
           ttDocumentAttachment.seq         = iSeq
           ttDocumentAttachment.description = pDescription
           ttDocumentAttachment.path        = pPath.
end.

procedure createDocumentAttachmentRest:
    def input parameter pUid          as char no-undo.
    def input parameter pDescription  as char no-undo.
    def input parameter pPath         as char no-undo.
    def input parameter pURL          as char no-undo.
    def input parameter pAuth         as char no-undo.
    def input parameter pLastModified as char no-undo.
    def input parameter pPageSize     as int  no-undo.

    def buffer bTtDocumentAttachment for ttDocumentAttachment.

    def var iSeq as int  no-undo init 1.

    if pUid = "" or pURL = "" or pAuth = "" or pPath = "" then do:
        return.
    end.

    if can-find(first ttDocumentAttachment no-lock
                    where ttDocumentAttachment.docUid = pUid
                      and ttDocumentAttachment.path   = pPath) then do:
        return.
    end.
    
    for last bTtDocumentAttachment
        where bTtDocumentAttachment.docUid = pUid:
        assign iSeq = bTtDocumentAttachment.seq + 1.
    end.

    create ttDocumentAttachment.
    assign ttDocumentAttachment.docUid           = pUid
           ttDocumentAttachment.seq              = iSeq
           ttDocumentAttachment.description      = pDescription
           ttDocumentAttachment.path             = pPath
           ttDocumentAttachment.url              = pURL
           ttDocumentAttachment.rawHeaders       = pAuth
           ttDocumentAttachment.lastModified     = pLastModified
           ttDocumentAttachment.downloadPageSize = pPageSize.  
end.

procedure _copyDocument:
    def input parameter pIdSource as char   no-undo.
    def input parameter pIdDest   as char   no-undo.
    def input parameter pTtSource as handle no-undo.

    def var hQry       as handle no-undo.
    def var hBufSource as handle no-undo.
    def var hBufDest   as handle no-undo.

    assign hBufSource = pTtSource:default-buffer-handle.

    create query hQry.
    hQry:set-buffers(hBufSource).
    hQry:query-prepare("for each " + pTtSource:name + " where docUid = '" + pIdSource + "'").
    hQry:query-open().

    create buffer hBufDest for table hBufSource:name.

    repeat while hQry:get-next():
        hBufDest:buffer-create().
        hBufDest:buffer-copy(hBufSource, "docUid").
        hBufDest:buffer-field("docUid"):buffer-value = pIdDest.
    end.

    hQry:query-close().
    delete object hQry.
end.

procedure copyDocument:
    def input  parameter pUid            as char no-undo.
    def input  parameter pUserDest       as char no-undo.
    def input  parameter pIsAlternative  as log  no-undo.
    def output parameter pNewUid         as char no-undo.

    def buffer bTtDocument              for ttDocument.
    def buffer bTtDocumentMetadata      for ttDocumentMetadata.
    def buffer bTtDocumentItem          for ttDocumentItem.
    def buffer bTtDocumentItemMetadata  for ttDocumentItemMetadata.
    def buffer bTtDocumentTag           for ttDocumentTag.
    def buffer bTtDocumentAttachment    for ttDocumentAttachment.
    def buffer bTtTableMetadata         for ttTableMetadata.

    if pUid = "" or pUserDest = "" then do:
        return.
    end.

    for first ttDocument
        where ttDocument.uid = pUid:

        if can-find(first bTtDocument
                        where bTtDocument.companyId     = ttDocument.companyId
                          and bTtDocument.documentId    = ttDocument.documentId
                          and bTtDocument.type          = ttDocument.type
                          and bTtDocument.aprovadorUser = pUserDest) then do:
            return.
        end.

        pNewUid = ttDocument.companyId  + "_" +
                  ttDocument.documentId + "_" +
                  ttDocument.type       + "_" +
                  pUserDest.

        create bTtDocument.
        buffer-copy ttDocument 
            except uid aprovadorUser
                to bTtDocument
            assign bTtDocument.uid           = pNewUid
                   bTtDocument.aprovadorUser = pUserDest
                   bTtDocument.isAlternative = pIsAlternative.

        run _copyDocument(pUid, pNewUid, temp-table ttDocumentMetadata:handle).
        run _copyDocument(pUid, pNewUid, temp-table ttDocumentItem:handle).
        run _copyDocument(pUid, pNewUid, temp-table ttDocumentItemMetadata:handle).
        run _copyDocument(pUid, pNewUid, temp-table ttDocumentTag:handle).
        run _copyDocument(pUid, pNewUid, temp-table ttDocumentAttachment:handle).
        run _copyDocument(pUid, pNewUid, temp-table ttTableMetadata:handle).
    end.
end.
