# Includes Progress

Includes com definições para as customizações do Aprovador em Progress:

* **documents.i**: temp-tables e procedures para criação de documentos;
* **aprovadorLib.i**: helpers para formatação de data, moeda, etc;
* **fnd.i**: procedures para conexão e desconexão dos bancos por empresa para o Totvs Datasul;

# Aprovador

Para saber mais sobre o Aprovador, acesse [https://aprovador.com](https://aprovador.com). Para saber como customizar o Aprovador, acesse o [Portal do Desenvolvedor](https://aprovador.com/documentacao/).

# Licença

Os fontes disponíveis neste repositório são licenciados sob a [Licença MIT](https://opensource.org/licenses/MIT).

---

_Totvs Datasul é uma marca registrada da Totvs S.A_.