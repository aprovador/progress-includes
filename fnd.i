/**
 * Aprovador - conexao e desconexao dos bancos da empresa
 * fnd.i
 */ 

procedure getDbNames:
    def output parameter pBcoLogic       as char no-undo.
    def output parameter pBcoFisic       as char no-undo.
    def output parameter pBcoLogicOracle as char no-undo.

    def var iCont    as int  no-undo.
    def var cDb      as char no-undo.
    def var iPos     as int  no-undo.
    def var lLogic   as log  no-undo init no.

    if not avail bco_empres then do:
        return.
    end.

    if bco_empres.log_bco_oracle then do:
        do iCont = 1 to length(bco_empres.cod_param_conex):
            if substr(bco_empres.cod_param_conex, iCont, 3) = '-ld':U then do:
                if lLogic = no then do:
                    assign pBcoLogic = substr(bco_empres.cod_param_conex,iCont + 4)
                           iPos      = index(pBcoLogic,' ':U)
                           pBcoLogic = substr(pBcoLogic, 1, iPos - 1)
                           pBcoLogic = trim(pBcoLogic)
                           lLogic    = yes.
                    if bco_empres.log_bco_unif = no then do:
                        leave.
                    end.
                end.
                else do:
                    assign pBcoLogicOracle = substr(bco_empres.cod_param_conex, iCont + 4)
                           iPos            = index(pBcoLogicOracle,' ':U)
                           pBcoLogicOracle = substr(pBcoLogicOracle, 1, iPos - 1)
                           pBcoLogicOracle = trim(pBcoLogicOracle).
                    leave.
                end.
            end.
        end.
    end.
    else do:
        assign pBcoLogic = trim(bco_empres.cod_bco_logic)
               pBcoFisic = trim(bco_empres.cod_bco_fisic).
    end.
end.

procedure connectCompany:
    def input  parameter pCompany as char no-undo.
    def output parameter pError   as char no-undo.

    def var iCont           as int  no-undo.
    def var cBcoLogic       as char no-undo.
    def var cBcoFisic       as char no-undo.
    def var cBcoLogicOracle as char no-undo.
    def var cAlias          as char no-undo.

    if not can-find(first fnd_empres no-lock
                        where fnd_empres.cod_empresa = pCompany) then do:
        pError = "Nao encontrada empresa foundation com o codigo " + pCompany + ".".
        return.
    end.

    for each bco_empres no-lock
        where bco_empres.cod_empresa = pCompany:

        if bco_empres.idi_tip_bco = 3 then do:
            next.
        end.

        run getDbNames(output cBcoLogic,
                       output cBcoFisic,
                       output cBcoLogicOracle).

        if cBcoLogic = "emsfnd" then do:
            next.
        end.

        if connected(cBcoLogic) or connected(cBcoLogicOracle) then do:
            next.
        end.

        if cBcoLogic matches "*eai*"      or
           cBcologic matches "*dtviewer*" then do:
            next.
        end.

        if bco_empres.log_bco_oracle then do:
            connect value(bco_empres.cod_param_conex) -ct 5 no-error.
        end.
        else do:
            connect value(cBcoFisic) "-ld" value(cBcoLogic) value(bco_empres.cod_param_conex) -ct 5 no-error.
        end.

        if not connected(cBcoLogic) then do:
            assign pError = pError + " | " + 
                            "Nao foi possivel conectar ao banco: " + cBcoLogic + ". " +
                            "Erro: " + error-status:get-message(1).
        end.
        else do:
            /* Create aliases */
            do iCont = 1 to num-entries(bco_empres.cod_alias_bco):
                cAlias = trim(entry(iCont, bco_empres.cod_alias_bco)).
                
                if bco_empres.log_bco_oracle then do:
                    create alias value(cAlias) for database value(cBcoLogicOracle) no-error.
                    create alias value("sh":U + cAlias) for database value(cBcoLogic) no-error.
                end.
                else do:
                    create alias value(cAlias) for database value(cBcoLogic) no-error.
                end.
            end.
        end.
    end.

    assign pError = trim(pError)
           pError = trim(pError, "|").
end.

procedure disconnectCompany:
    def input  parameter pCompany as char no-undo.

    def var iCont     as int  no-undo.
    def var iAlias    as int  no-undo.
    def var cDbName   as char no-undo.

    /* Elimina handles da memoria, pois podem estar travando bancos ao desconectar. */
    do while valid-handle(session:first-procedure):
        delete procedure session:first-procedure.
    end.

    do iCont = 1 To Num-dbs:
        assign cDbName = ldbname(iCont).

        if cDbName = "emsfnd" or cDbName = "shemsfnd" then do:
            next.
        end.

        if not connected(cDbName) then do:
            next.
        end.

        /* Delete aliases */
        do iAlias = 1 to num-alias:
            if cDbName = ldbname(alias(iAlias)) then do:
                delete alias value(alias(iAlias)).
                /* para evitar pular o alias ao eliminar */
                assign iAlias = iAlias - 1. 
            end.
        end.

        disconnect value(cDbName) no-error.

        /* para evitar pular o banco ao desconectar */
        assign iCont = iCont - 1.
    end.
end.

/* Funcao para verificar alguns bancos basicos que indicam que o EMS 2 e 5
   estao conectados. */
function isEMSConnected return logical:
    if connected("movind") and
       connected("emsfin") then do: 
        return true.
    end.
    return false.
end.
