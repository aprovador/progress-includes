/**
 * Aprovador - i18n.i
 *
 */

&global-define I18N
&global-define I18N_SEP #
&global-define LANG_PTBR pt-br
&global-define LANG_EN en
&global-define LANG_ES es
&global-define DEFAULT_LANG pt-br

def new global shared temp-table ttI18nString_G no-undo
    field language as char
    field strKey   as char
    field strValue as char
    index pk as primary unique
          language
          strKey.

/* Functions */
function getI18nString returns char (pLanguage as char,
                                     pKey      as char):
    def var cValue as char no-undo.

    for first ttI18nString_G
        where ttI18nString_G.language = pLanguage
          and ttI18nString_G.strKey   = pKey:
        assign cValue = ttI18nString_G.strValue.
    end.

    return cValue.
end.

/* Interpolation format: "Exemplo {1}: {2}" */
function getI18nStringI returns char (pLanguage as char,
                                      pKey      as char,
                                      pValues   as char):
    def var cStr  as char no-undo.
    def var iCont as int  no-undo.

    assign cStr = getI18NString(pLanguage, pKey).
    if cStr <> "" then do:
        do iCont = 1 to num-entries(pValues, "{&I18N_SEP}"):
            assign cStr = replace(cStr, "~{" + string(iCont) + "~}", entry(iCont, pValues, "{&I18N_SEP}")).
        end.
    end.

    return cStr.
end.

function i18n_dateFormat returns char (pLanguage as char,
                                       pDate as date):
    if pDate = ? then do:
        return "".
    end.

    case pLanguage:
    when "{&LANG_PTBR}" or when "{&LANG_ES}" then do:
        return string(day(pDate), "99") + "/" + string(month(pDate), "99") + "/" + string(year(pDate)).
    end.
    when "{&LANG_EN}" then do:
        return string(month(pDate)) + "/" + string(day(pDate)) + "/" + string(year(pDate)).
    end.
    end case.
end.

function i18n_datetimeFormat returns char (pLanguage as char,
                                           pDate     as datetime):
    def var cDate as char no-undo.

    assign cDate = i18n_dateFormat(pLanguage, date(pDate))
           cDate = cDate + " " + string(int(mtime(pDate) / 1000), "hh:mm:ss").

    return cDate.
end.

/* Procedure */
procedure loadI18nStrings:
    def input  parameter pLanguage as char no-undo.
    def input  parameter pFilename as char no-undo.
    def output parameter pError    as char no-undo.

    def var cLine  as char no-undo.
    def var cKey   as char no-undo.
    def var cValue as char no-undo.
    def var iIndex as int  no-undo.
    def var cPath  as char no-undo.

    assign cPath = search(pFilename).

    if cPath = ? then do:
        assign pError = "Error reading translation file for language '" + pLanguage + "': " + pFilename + ".".
        return.
    end.

    if pLanguage = "" then do:
        assign pLanguage = "{&LANG_PTBR}".
    end.

    input from value(cPath) convert source 'utf-8'.
    repeat:
        import unformatted cLine.
        
        assign cLine  = trim(trim(cLine), ",")
               iIndex = index(cLine, ":").
        
        if iIndex = 0 then do:
            next.
        end.

        assign cKey   = trim(substr(cLine, 1, iIndex - 1))
               cKey   = substr(cKey, 2, length(cKey) - 2)
               cValue = trim(substr(cLine, iIndex + 1))
               cValue = substr(cValue, 2, length(cValue) - 2).

        find first ttI18nString_G
             where ttI18nString_G.language = pLanguage
               and ttI18nString_G.strKey   = cKey no-error.
        if not avail ttI18nString_G then do:
            create ttI18nString_G.
            assign ttI18nString_G.language = pLanguage
                   ttI18nString_G.strKey   = cKey.
        end.

        assign ttI18nString_G.strValue = cValue.
    end.
    input close.
end.
