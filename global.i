/**
 * Aprovador Sistemas LTDA - 2015
 * All rights reserved.
 *
 * global.i
 */
 
&global-define APROVADOR-GLOBAL
 
def new global shared var APROVADOR_DEBUG_ENABLED as log  no-undo init no.
def new global shared var APROVADOR_SANDBOX_DIR   as char no-undo.
